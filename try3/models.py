from django.db import models


class MasterState(models.Model):
    id = models.IntegerField(primary_key=True)
    lgd_code = models.IntegerField()
    state_name = models.CharField(max_length=250)
    state_short_code = models.CharField(max_length=100, blank=True, null=True)
    country_id = models.IntegerField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    created_by = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    modified_by = models.IntegerField(blank=True, null=True)
    modified_at = models.DateTimeField(blank=True, null=True)
    objects = models.Manager()

    def __str__(self):
        return self.state_name


class Sponsor_bank(models.Model):
    bank_name = models.CharField(max_length=200)
    postal_address = models.CharField(max_length=500)
    rseti_department = models.CharField(max_length=200)
    department_address = models.CharField(max_length=200)
    top_executives = models.CharField(max_length=200)
    landline_number = models.IntegerField()
    address = models.CharField(max_length=200)

    def __str__(self):
        return self.bank_name


class TrustAccountDetails(models.Model):
    name_of_the_trust = models.CharField(max_length=200)
    house_no = models.CharField(max_length=200)
    street = models.CharField(max_length=200)
    village = models.CharField(max_length=200)
    state = models.IntegerField()
    district = models.CharField(max_length=200)
    pincode = models.CharField(max_length=200)
    bank_acc_no = models.CharField(max_length=200)
    bank_name = models.CharField(max_length=200)
    back_acc_type = models.CharField(max_length=200)
    back_acc_ifs = models.CharField(max_length=200)
    registration_certificate = models.CharField(max_length=2000)
    pan_card = models.CharField(max_length=2000)
    it_exemption = models.CharField(max_length=2000)
    mou_file = models.CharField(max_length=2000)

    def __str__(self):
        return self.name_of_the_trust
