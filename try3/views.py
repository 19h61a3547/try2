from django.shortcuts import render

# Create your views here.
from try3.models import MasterState, TrustAccountDetails, Sponsor_bank


def index(res):
    content = {
        'states': MasterState.objects.all()
    }
    return render(res, 'trust-details.html', content)


def newPage(request):
    bank_name = request.POST.get("bank_name")
    postal_address = request.POST.get('postal_address')
    rseti_department = request.POST.get('rseti_department')
    department_address = request.POST.get('department_address')
    top_executives = request.POST.get('top_executives')
    landline_number = request.POST.get('landline_number')
    address = request.POST.get('address')

    name_of_the_trust = request.POST.get('name_of_the_trust')
    house_no = request.POST.get('house_no')
    street = request.POST.get('street')
    village = request.POST.get('village')
    state = request.POST.get('state')
    district = request.POST.get('district')
    pincode = request.POST.get('pincode')
    bank_acc_no = request.POST.get('bank_acc_no')
    branch_name = request.POST.get('bank_name')
    back_acc_type = request.POST.get('back_acc_type')
    back_acc_ifs = request.POST.get('back_acc_ifs')
    registration_certificate = request.POST.get('registration_certificate')
    pan_card = request.POST.get('pan_card')
    it_exemption = request.POST.get('it_exemption')
    mou_file = request.POST.get('mou_file')

    TrustAccountDetails(name_of_the_trust=name_of_the_trust, house_no=house_no, street=street, village=village,
                        state=state, district=district, pincode=pincode, bank_acc_no=bank_acc_no, bank_name=branch_name,
                        back_acc_type=back_acc_type, back_acc_ifs=back_acc_ifs,
                        registration_certificate=registration_certificate, pan_card=pan_card, it_exemption=it_exemption,
                        mou_file=mou_file).save()
    Sponsor_bank(bank_name=bank_name, postal_address=postal_address, rseti_department=rseti_department,
                 department_address=department_address, top_executives=top_executives, landline_number=landline_number,
                 address=address).save()
    return index(request)
