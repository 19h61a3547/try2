from django.urls import path

from try3 import views

urlpatterns = [
    path('', views.index),
    path('new/', views.newPage),
]
