from django.contrib import admin

# Register your models here.
from try3.models import MasterState, Sponsor_bank, TrustAccountDetails

admin.site.register(MasterState)
admin.site.register(Sponsor_bank)
admin.site.register(TrustAccountDetails)