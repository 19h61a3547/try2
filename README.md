***Created a new project***
---
1. `python -m venv env`
2. (initial commit)
3. (add README.md)
4. `env\Scripts\activate`\
---
Started a Project
---
5. `pip install django`
6. installed django
7. `django-admin startproject webproj .`
8. `python manage.py startapp try3`\
---
Added HTML files
---
9. added html file and other files(static)
10. `python manage.py collectstatic`\
---
Connecting to the database
---
11. add a model
12. add objects=models.Manager()
13. `python manage.py makemigrations`
14. `python manage.py migrate`
---
***Task Completed***
---



